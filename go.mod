module gitlab.com/guap-programming/2/saod/labs/lab2

go 1.13

require (
	github.com/spf13/cobra v0.0.5
	gitlab.com/otus_golang/05_doublylinkedlist v0.0.1
)
