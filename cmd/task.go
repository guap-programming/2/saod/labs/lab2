package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/guap-programming/2/saod/labs/lab2/utils"
	doublyLinkedList "gitlab.com/otus_golang/05_doublylinkedlist"
)

var from int
var to int

func init() {
	task.PersistentFlags().IntVarP(&from, "from", "f", 0, "from")
	task.PersistentFlags().IntVarP(&to, "to", "t", 0, "to")

	rootCmd.AddCommand(task)
}

var task = &cobra.Command{
	Use:   "task",
	Short: "task",
	Long:  `task`,
	Run: func(cmd *cobra.Command, args []string) {
		l := utils.GenerateRandomNumbersList(10, 100)
		utils.PrintListPretty(l)

		resultList := &doublyLinkedList.List{}
		for i := l.First(); i != nil; i = i.Next() {
			if i.Value.(int) >= from && i.Value.(int) <= to {
				resultList.PushBack(i.Value)
			}
		}
		fmt.Print("\nResult ")
		utils.PrintListPretty(resultList)
	},
}
