package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/guap-programming/2/saod/labs/lab2/utils"
)

var addNum int

func init() {
	add.PersistentFlags().IntVarP(&addNum, "number", "n", 0, "number to add")

	rootCmd.AddCommand(add)
}

var add = &cobra.Command{
	Use:   "add",
	Short: "add number",
	Long:  `add number`,
	Run: func(cmd *cobra.Command, args []string) {
		numbersList := utils.GenerateRandomNumbersList(10, 100)
		utils.PrintListPretty(numbersList)

		fmt.Printf("\nadding number: %d\n", addNum)
		numbersList.PushBack(addNum)

		fmt.Print("\nResult ")
		utils.PrintListPretty(numbersList)
	},
}
