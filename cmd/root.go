package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
)

var rootCmd = &cobra.Command{
	Use:   "lab2",
	Short: "lab2",
	Long:  `lab2`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print("Use lab2 [command]\nRun 'lab2 --help' for usage.\n")
	},
}

// Execute is starting application
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("root execute error: %v", err)
	}
}
