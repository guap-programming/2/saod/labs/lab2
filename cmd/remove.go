package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/guap-programming/2/saod/labs/lab2/utils"
)

var removeNum int

func init() {
	remove.PersistentFlags().IntVarP(&removeNum, "number", "n", 0, "number to remove")

	rootCmd.AddCommand(remove)
}

var remove = &cobra.Command{
	Use:   "remove",
	Short: "remove number",
	Long:  `remove number`,
	Run: func(cmd *cobra.Command, args []string) {
		l := utils.GenerateRandomNumbersList(10, 10)
		utils.PrintListPretty(l)

		fmt.Printf("\nremoving number: %d\n", removeNum)
		for i := l.First(); i != nil; i = i.Next() {
			if i.Value.(int) == removeNum {
				i.Remove()
				break
			}
		}

		fmt.Print("\nResult ")
		utils.PrintListPretty(l)
	},
}
