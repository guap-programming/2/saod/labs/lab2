package main

import (
	"gitlab.com/guap-programming/2/saod/labs/lab2/cmd"
)

func main() {
	cmd.Execute()
}
