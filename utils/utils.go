package utils

import (
	"fmt"
	doublyLinkedList "gitlab.com/otus_golang/05_doublylinkedlist"
	"math/rand"
	"time"
)

func GenerateRandomNumbersList(len int, maxNum int) *doublyLinkedList.List {
	rand.Seed(time.Now().UTC().UnixNano())
	list := &doublyLinkedList.List{}
	for i := 0; i < len; i++ {
		list.PushFront(rand.Int() % maxNum)
	}
	return list
}

func PrintListPretty(l *doublyLinkedList.List) {
	if l.Len() == 0 {
		fmt.Print("list is empty\n")
		return
	}

	fmt.Print("List:\n")
	for i := l.First(); i != nil; i = i.Next() {
		fmt.Print(i.Value, " ")
	}
	fmt.Print("\n")
}
